# Compiling

1. Install the ALE framework:

    ```
    git clone git@github.com:mgbellemare/Arcade-Learning-Environment.git
    ```

2. Get the backgrounds (no need to compile) from:

    ```
    git clone git@github.com:mcmachado/ALEResearch.git
    ```

3. Edit the path in `bpro.cfg`
4. Make sure that `libale.so` is on the path. In my case:

    ```
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/workspace/Arcade-Learning-Environment
    ```

5. Compile the shallowpy wrapper and Marlos' code with `make`
