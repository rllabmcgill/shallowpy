import time
import numpy as np

from shallow import BlobTimeFeatures
from ale_python_interface import ale_python_interface as ALE

lr = 1e-4
eps = 0.01
discount = 0.9
nepisodes = 1000
rng = np.random.RandomState(2468)

ale = ALE.ALEInterface()
ale.setInt(b'random_seed', 2468)
ale.setInt(b'frame_skip', 5)
ale.setBool(b'color_averaging', True);
ale.loadROM(b'roms/asterix.bin')

blobprost = BlobTimeFeatures('-s 2468 -c bpro.cfg -r roms/asterix.bin')
blobprost.clearCache()

legal_actions = ale.getLegalActionSet()
weights = np.zeros((blobprost.getNumberOfFeatures(), len(legal_actions)), dtype="float32")

def egreedy(phi):
     if rng.uniform() < eps:
         return rng.choice(range(len(legal_actions)))
     return np.sum(weights[phi], axis=0).argmax()

cumrewards = []
for i in range(nepisodes):
    cumreward = 0.
    start_time = time.time()

    valueprev = 0.
    phi = np.copy(blobprost.getActiveFeaturesIndices(ale))
    action = egreedy(phi)

    while not ale.game_over():
        reward = ale.act(legal_actions[action])
        phiprime = blobprost.getActiveFeaturesIndices(ale)
        actionprime = egreedy(phiprime)

        # SARSA(0) update
        value = weights[phiprime, actionprime].sum()
        tderror = reward +  discount*value - valueprev
        weights[phi, action] += lr*tderror

        phi = np.copy(phiprime)
        action = actionprime
        valueprev = value
        cumreward += reward

    cumrewards.append(cumreward)
    print('Episode {}: Cumulative Reward: {} Avg. Reward: {} Frames: {} FPS: {} '.format(i,\
                        cumreward, \
                        np.mean(cumrewards),\
                        ale.getEpisodeFrameNumber(), \
                        ale.getEpisodeFrameNumber() / (time.time() - start_time)))
    blobprost.clearCache()
    ale.reset_game()
