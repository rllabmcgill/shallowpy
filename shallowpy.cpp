#include "shallowpy.hpp"
#include "features/BlobTimeFeatures.hpp"
#include "features/BasicFeatures.hpp"
#include "features/BASSFeatures.hpp"

Parameters* Parameters_new(int argc, char** argv) {
  return new Parameters(argc, argv);
}

ShallowFeaturesData* ShallowFeaturesData_new(features_t features_type, Parameters *param)
{
  ShallowFeaturesData* data = new ShallowFeaturesData();
  switch(features_type) {
    case BLOB_PROST:
      data->self = new BlobTimeFeatures(param);
      break;
    case BASIC:
      data->self = new BasicFeatures(param);
      break;
    case BASS:
      data->self = new BASSFeatures(param);
      break;
    default:
      data->self = new BlobTimeFeatures(param);
  }
  return data;
}

long long* ShallowFeatures_getActiveFeaturesIndices(ShallowFeaturesData* data, ALEInterface* ale)
{
  data->features.clear();
  data->self->getActiveFeaturesIndices(ale->getScreen(), ale->getRAM(), data->features);
  return data->features.data();
}

unsigned long long ShallowFeatures_getFeatureSize(ShallowFeaturesData* data)
{
  return data->features.size();
}

long long ShallowFeatures_getNumberOfFeatures(ShallowFeaturesData* data)
{
  return data->self->getNumberOfFeatures();
}

void ShallowFeatures_clearCache(ShallowFeaturesData* data)
{
  data->self->clearCache();
}
