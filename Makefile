USE_SDL     := 1
CFLAGS = `pkg-config --cflags libale`
LDFLAGS = `pkg-config --libs libale`

ifeq ($(strip $(USE_SDL)), 1)
  CFLAGS +=  `sdl-config --cflags --libs`
  LDFLAGS += -lSDL -lSDL_gfx -lSDL_image
endif

CXX := g++ -std=c++11 -fPIC -O3

all: libshallowale.so

bin/Parameters.o: features/Parameters.cpp
	$(CXX) $(CFLAGS) -c features/Parameters.cpp -o bin/Parameters.o

bin/Features.o: features/Features.cpp
	$(CXX) $(CFLAGS) -c features/Features.cpp -o bin/Features.o

bin/Background.o: features/Background.cpp
	$(CXX) $(CFLAGS) -c features/Background.cpp -o bin/Background.o

bin/BlobTimeFeatures.o: features/BlobTimeFeatures.cpp
	$(CXX) $(CFLAGS) -c features/BlobTimeFeatures.cpp -o bin/BlobTimeFeatures.o

bin/BasicFeatures.o: features/BasicFeatures.cpp features/BasicFeatures.hpp
		$(CXX) $(CFLAGS) -c features/BasicFeatures.cpp -o bin/BasicFeatures.o

bin/BASSFeatures.o: features/BASSFeatures.cpp features/BASSFeatures.hpp
		$(CXX) $(CFLAGS) -c features/BASSFeatures.cpp -o bin/BASSFeatures.o

bin/shallowpy.o: shallowpy.cpp
	$(CXX) $(CFLAGS) -c shallowpy.cpp $(LDFLAGS) -o bin/shallowpy.o

libshallowale.so: bin/shallowpy.o bin/BlobTimeFeatures.o bin/Background.o bin/Features.o bin/Parameters.o bin/BasicFeatures.o bin/BASSFeatures.o
	 $(CXX) -shared -Wl,-soname,libshallowale.so -o libshallowale.so bin/shallowpy.o bin/Features.o bin/Background.o bin/BasicFeatures.o bin/BASSFeatures.o bin/BlobTimeFeatures.o bin/Parameters.o -L$(ALE) -lale

clean:
	rm -rf ${OUT_FILE} bin/*.o
	rm -f *.so
