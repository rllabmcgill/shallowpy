import os
import sys
import ctypes
from numpy.ctypeslib import as_array

lib = ctypes.cdll.LoadLibrary(os.path.join(os.path.dirname(__file__), 'libshallowale.so'))

lib.Parameters_new.argtypes = [ctypes.c_int, ctypes.POINTER(ctypes.c_char_p)]
lib.Parameters_new.restype = ctypes.c_void_p

lib.ShallowFeaturesData_new.argtypes = [ctypes.c_uint, ctypes.c_void_p,]
lib.ShallowFeaturesData_new.restype = ctypes.c_void_p

lib.ShallowFeatures_getNumberOfFeatures.argtypes = [ctypes.c_void_p,]
lib.ShallowFeatures_getNumberOfFeatures.restype = ctypes.c_longlong

lib.ShallowFeatures_getActiveFeaturesIndices.argtypes = [ctypes.c_void_p, ctypes.c_void_p]
lib.ShallowFeatures_getActiveFeaturesIndices.restype = ctypes.POINTER(ctypes.c_longlong)

lib.ShallowFeatures_getFeatureSize.argtypes = [ctypes.c_void_p,]
lib.ShallowFeatures_getFeatureSize.restype = ctypes.c_ulonglong

lib.ShallowFeatures_clearCache.argstypes = [ctypes.c_void_p,]
lib.ShallowFeatures_clearCache.restype = None

class ShallowFeatures:
    BLOB_PROST = 0
    BASIC = 1
    BASS = 2

    def __init__(self, features_type, args):
        args = bytes(sys.argv[0] + ' ' + args, 'UTF-8').split()
        argv = (ctypes.c_char_p*len(args))(*args)
        params = lib.Parameters_new(len(args), ctypes.cast(argv, ctypes.POINTER(ctypes.c_char_p)))
        self.obj = lib.ShallowFeaturesData_new(features_type, params)

    def getNumberOfFeatures(self):
        return lib.ShallowFeatures_getNumberOfFeatures(self.obj)

    def getActiveFeaturesIndices(self, ale):
        obj = lib.ShallowFeatures_getActiveFeaturesIndices(self.obj, ale.obj)
        size = lib.ShallowFeatures_getFeatureSize(self.obj)
        return as_array(obj, shape=(size,))

    def clearCache(self):
        lib.ShallowFeatures_clearCache(self.obj)

class BlobTimeFeatures(ShallowFeatures):
    def __init__(self, args):
        super(BlobTimeFeatures, self).__init__(self.BLOB_PROST, args)

class BasicFeatures(ShallowFeatures):
    def __init__(self, args):
        super(BasicFeatures, self).__init__(self.BASIC, args)

class BASSFeatures(ShallowFeatures):
    def __init__(self, args):
        super(BASSFeatures, self).__init__(self.BASS, args)
