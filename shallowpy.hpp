#ifndef __SHALLOWPY_HPP__
#define __SHALLOWPY_HPP__

#include "features/Parameters.hpp"
#include "features/Features.hpp"

#include <vector>
#include <ale_interface.hpp>

extern "C" {
  typedef enum {
    BLOB_PROST,
    BASIC,
    BASS
  } features_t;

  struct ShallowFeaturesData
  {
    Features* self;
    std::vector<long long> features;
  };

  Parameters* Parameters_new(int argc, char** argv);
  ShallowFeaturesData* ShallowFeaturesData_new(features_t features_type, Parameters *param);

  long long* ShallowFeatures_getActiveFeaturesIndices(ShallowFeaturesData* data, ALEInterface* ale);
  long long ShallowFeatures_getNumberOfFeatures(ShallowFeaturesData* data);
  unsigned long long ShallowFeatures_getFeatureSize(ShallowFeaturesData* data);
  void ShallowFeatures_clearCache(ShallowFeaturesData* data);

}

#endif
